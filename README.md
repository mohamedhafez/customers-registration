
### Sequence Diagram
![Sequence Diagram](sequence-diagram.jpeg)

### Class diagram
![Class Diagram](class-diagram.png)

## Installation
- Run `make build`

## Run test cases
- Run `make test`

### Customer registration form
- GET `http://localhost:8001/customers/register`

### database access
- Hit `http://localhost:8080/?server=database-container&username=root&db=wunder`
- password `sqlpass`


### TODO
- Increase the fault tolerance while storing the payment details by implement retry pattern in case `wunderfleet api` down.
- Apply customer registration status as `DRAFT, PENDING_ADDRESS, PENDING_PAYMENT` => `src/Service/Constant/RegistrationStatus.php`
- Add logging system for each step (status/registration life cycle).
- Add validation layer.

### Built With
* [PHP7.4](http://php.net)
* [Docker](https://www.docker.com/)
* [Symfony5](http://www.symfony.com)
    * Symfony/phpunit-bridge
    * Doctrine