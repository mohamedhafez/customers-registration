<?php

namespace App\Controller;

use App\Form\RegistrationFormType;
use App\Service\Contract\RegistrationServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RegistrationController
 *
 * @Route("/customers")
 *
 * @package App\Controller
 */
class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="registration_index", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $registrationForm = $this->createForm(RegistrationFormType::class, null, [
            'action' => $this->generateUrl('registration_complete')
        ]);

        return $this->render('registration/index.html.twig', [
            'registration' => $registrationForm->createView(),
        ]);
    }

    /**
     * @Route("/register", name="registration_complete", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request, RegistrationServiceInterface $registrationService)
    {
        $form = $this->createForm(RegistrationFormType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() && !$form->isValid()) {
            return $this->handleView($this->view($form->getErrors()));
        }

        $registrationService->register($form->getData());
        $this->addFlash('success', 'Registration successfully!');

        return $this->redirect($this->generateUrl('registration_index'), Response::HTTP_CREATED);
    }
}
