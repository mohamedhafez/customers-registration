<?php declare(strict_types=1);

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressFormType
 *
 * @package App\Form
 */
class AddressFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street_name', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Street Name'],
            ])
            ->add('house_number', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'House Number'],
            ])
            ->add('zip_code', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Zip Code'],
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'City'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
            'csrf_protection' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
