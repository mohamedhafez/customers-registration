<?php declare(strict_types=1);

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CustomerFormType
 *
 * @package App\Form
 */
class CustomerFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'First Name'],
            ])
            ->add('last_name', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Last Name'],
            ])
            ->add('telephone', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Telephone Number'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'csrf_protection' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
