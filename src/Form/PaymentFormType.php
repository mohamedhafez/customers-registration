<?php declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentFormType
 *
 * @package App\Form
 */
class PaymentFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Account Owner'],
            ])
            ->add('iban', TextType::class, [
                'required' => true,
                'trim' => true,
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'IBAN'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => true,
        ));
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
