<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Address;
use App\Repository\Contract\AddressRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

/**
 * Class AddressRepository
 *
 * @package App\Repository
 */
class AddressRepository implements AddressRepositoryInterface
{
    private ObjectRepository $repository;

    private EntityManagerInterface $entityManager;

    /**
     * AddressRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Address::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param Address $address
     *
     * @return Address
     */
    public function create(Address $address): Address
    {
        $this->entityManager->persist($address);
        $this->entityManager->flush();

        return $address;
    }
}
