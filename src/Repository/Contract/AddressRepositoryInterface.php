<?php declare(strict_types=1);

namespace App\Repository\Contract;

use App\Entity\Address;

/**
 * Interface AddressRepositoryInterface
 *
 * @package App\Repository\Contract
 */
interface AddressRepositoryInterface
{
    /**
     * @param \App\Entity\Address $data
     *
     * @return \App\Entity\Address
     */
    public function create(Address $data): Address;
}
