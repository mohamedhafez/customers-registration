<?php declare(strict_types=1);

namespace App\Repository\Contract;

use App\Entity\Customer;

/**
 * Interface CustomerRepositoryInterface
 *
 * @package App\Repository\Contract
 */
interface CustomerRepositoryInterface
{
    /**
     * @param Customer $data
     *
     * @return Customer
     */
    public function create(Customer $data): Customer;
}
