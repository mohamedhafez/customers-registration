<?php declare(strict_types=1);

namespace App\Repository\Contract;

use App\Entity\Payment;

/**
 * Interface PaymentRepositoryInterface
 *
 * @package App\Repository\Contract
 */
interface PaymentRepositoryInterface
{
    /**
     * @param Payment $payment
     *
     * @return mixed
     */
    public function create(Payment $payment): Payment;
}
