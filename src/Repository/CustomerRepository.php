<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Customer;
use App\Repository\Contract\CustomerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

/**
 * Class CustomerRepository
 *
 * @package App\Repository
 */
class CustomerRepository implements CustomerRepositoryInterface
{
    private ObjectRepository $repository;

    private EntityManagerInterface $entityManager;

    /**
     * CustomerRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Customer::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param Customer $customer
     *
     * @return Customer
     */
    public function create(Customer $customer): Customer
    {
        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $customer;
    }
}
