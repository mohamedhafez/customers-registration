<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Payment;
use App\Repository\Contract\PaymentRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

/**
 * Class PaymentRepository
 *
 * @package App\Repository
 */
class PaymentRepository implements PaymentRepositoryInterface
{
    private ObjectRepository $repository;

    private EntityManagerInterface $entityManager;

    /**
     * PaymentRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Payment::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param Payment $payment
     *
     * @return Payment
     */
    public function create(Payment $payment): Payment
    {
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $payment;
    }
}
