<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Address;
use App\Repository\Contract\AddressRepositoryInterface;
use App\Service\Contract\AddressServiceInterface;

/**
 * Class AddressService
 *
 * @package App\Service
 */
class AddressService implements AddressServiceInterface
{
    private AddressRepositoryInterface $repository;

    /**
     * ChannelService constructor.
     *
     * @param AddressRepositoryInterface $repository
     */
    public function __construct(AddressRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Address $address
     *
     * @return Address
     */
    public function create(Address $address): Address
    {
        return $this->repository->create($address);
    }
}
