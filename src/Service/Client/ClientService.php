<?php declare(strict_types=1);

namespace App\Service\Client;

use App\Service\Client\Contract\ClientInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ClientService
 *
 * @package App\Service\Client
 */
class ClientService implements ClientInterface
{
    private HttpClientInterface $httpClient;
    private array $config;

    /**
     * ClientService constructor.
     *
     * @param HttpClientInterface $client
     * @param array $config
     */
    public function __construct(HttpClientInterface $client, array $config)
    {
        $this->httpClient = $client;
        $this->config = $config;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function post(array $data): array
    {
        try {
            $response = $this->httpClient->request(
                $this->config['method'],
                $this->config['url'],
                [
                    'json' => $data
                ]
            );

            return $response->toArray();

        } catch (\Throwable $exception) {
            // TODO:: Use retry pattern OR store the data for dead later queue
            // TODO:: Log the error $exception->getMessage();

            return [];
        }
    }
}
