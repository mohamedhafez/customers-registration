<?php declare(strict_types=1);

namespace App\Service\Client\Contract;

/**
 * Interface ClientInterface
 *
 * @package App\Service\Client\Contract
 */
interface ClientInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function post(array $data);
}
