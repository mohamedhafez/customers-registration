<?php declare(strict_types=1);

namespace App\Service\Constant;

/**
 * Class RegistrationStatus
 *
 * @package App\Service\Constant
 */
final class RegistrationStatus
{
    const DRAFT = 1;
    const PENDING_ADDRESS = 2;
    const PENDING_PAYMENT = 3;
    const COMPLETED = 4;
}
