<?php declare(strict_types=1);

namespace App\Service\Contract;

use App\Entity\Address;

/**
 * Interface AddressServiceInterface
 *
 * @package App\Service\Contract
 */
interface AddressServiceInterface
{
    /**
     * @param Address $address
     *
     * @return mixed
     */
    public function create(Address $address): Address;
}
