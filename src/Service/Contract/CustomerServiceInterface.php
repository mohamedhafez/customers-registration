<?php declare(strict_types=1);

namespace App\Service\Contract;

use App\Entity\Customer;

/**
 * Interface CustomerServiceInterface
 *
 * @package App\Service\Contract
 */
interface CustomerServiceInterface
{
    /**
     * @param \App\Entity\Customer $data
     *
     * @return \App\Entity\Customer
     */
    public function create(Customer $data): Customer;
}
