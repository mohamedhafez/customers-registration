<?php declare(strict_types=1);

namespace App\Service\Contract;

/**
 * Interface PaymentServiceInterface
 *
 * @package App\Service\Contract
 */
interface PaymentServiceInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data): void;
}
