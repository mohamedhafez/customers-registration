<?php declare(strict_types=1);

namespace App\Service\Contract;

/**
 * Interface RegistrationServiceInterface
 *
 * @package App\Service\Contract
 */
interface RegistrationServiceInterface
{
    /**
     * @param array $data
     */
    public function register(array $data): void;
}
