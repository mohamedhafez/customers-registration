<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Customer;
use App\Repository\Contract\CustomerRepositoryInterface;
use App\Service\Contract\CustomerServiceInterface;

/**
 * Class CustomerService
 *
 * @package App\Service
 */
class CustomerService implements CustomerServiceInterface
{
    protected CustomerRepositoryInterface $repository;

    /**
     * CustomerService constructor.
     *
     * @param CustomerRepositoryInterface $repository
     */
    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Customer $customer
     *
     * @return Customer
     */
    public function create(Customer $customer): Customer
    {
        return $this->repository->create($customer);
    }
}
