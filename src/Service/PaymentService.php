<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Payment;
use App\Repository\Contract\PaymentRepositoryInterface;
use App\Service\Client\Contract\ClientInterface;
use App\Service\Contract\PaymentServiceInterface;

/**
 * Class PaymentService
 *
 * @package App\Service
 */
class PaymentService implements PaymentServiceInterface
{
    private PaymentRepositoryInterface $repository;
    private ClientInterface $client;

    /**
     * PaymentService constructor.
     *
     * @param PaymentRepositoryInterface $repository
     * @param ClientInterface $client
     */
    public function __construct(PaymentRepositoryInterface $repository, ClientInterface $client)
    {
        $this->repository = $repository;
        $this->client = $client;
    }

    /**
     * @param array $data
     */
    public function create(array $data): void
    {
        $customer = $data['customer'];
        $data['customerId'] = $customer->getId();

        $response = $this->client->post($data);

        if (isset($response['paymentDataId'])) {
            $payment = new Payment();
            $payment->setCustomer($customer)
                ->setPaymentDataId($response['paymentDataId'])
            ;
            $this->repository->create($payment);
        }
    }
}
