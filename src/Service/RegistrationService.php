<?php declare(strict_types=1);

namespace App\Service;

use App\Service\Contract\AddressServiceInterface;
use App\Service\Contract\CustomerServiceInterface;
use App\Service\Contract\PaymentServiceInterface;
use App\Service\Contract\RegistrationServiceInterface;

/**
 * Class RegistrationService
 *
 * @package App\Service
 */
class RegistrationService implements RegistrationServiceInterface
{
    protected CustomerServiceInterface $customerService;
    protected AddressServiceInterface $addressService;
    protected PaymentServiceInterface $paymentService;

    /**
     * RegistrationService constructor.
     *
     * @param CustomerServiceInterface $customerService
     * @param AddressServiceInterface  $addressService
     * @param PaymentServiceInterface $paymentService
     */
    public function __construct(
        CustomerServiceInterface $customerService,
        AddressServiceInterface $addressService,
        PaymentServiceInterface $paymentService
    ) {
        $this->customerService = $customerService;
        $this->addressService = $addressService;
        $this->paymentService = $paymentService;
    }

    /**
     * @param array $data
     */
    public function register(array $data): void
    {
        $customer = $this->customerService->create($data['customer']);

        $address = $data['address'];
        $address->setCustomer($customer);
        $this->addressService->create($address);

        $data['payment']['customer'] = $customer;
        $this->paymentService->create($data['payment']);
    }
}
