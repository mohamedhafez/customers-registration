<?php declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Entity\Address;
use App\Entity\Customer;
use App\Repository\Contract\AddressRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AddressService
 *
 * @package App\Tests\Unit\Service
 */
class AddressService extends WebTestCase
{
    public function testCreateAddress()
    {
        $address = $this->createAddress();
        $repository = $this->mockRepository($address);

        $addressService = new AddressService($repository);
        $result = $addressService->create($address);

        $this->assertInstanceOf(Address::class, $result);
    }

    public function createCustomer()
    {
        $customer = new Customer();
        $customer->setFirstName('Jone')
            ->setLastName('Doe')
            ->setTelephone('004919878976')
        ;

        return $customer;
    }

    public function createAddress()
    {
        $address = new Address();
        $address->setCity('Nurnberg')
            ->setZipCode(90553)
            ->setStreetName('Berliner strasse')
            ->setHouseNumber('13b')
            ->setCustomer($this->createCustomer())
        ;

        return $address;
    }

    public function mockRepository(Address $address)
    {
        $repository = $this->createMock(AddressRepositoryInterface::class);
        $repository
            ->expects($this->once())
            ->method('create')
            ->willReturn($address)
        ;

        return $repository;
    }
}
