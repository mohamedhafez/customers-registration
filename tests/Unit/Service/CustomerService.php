<?php declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Entity\Customer;
use App\Repository\Contract\CustomerRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CustomerService
 *
 * @package App\Tests\Unit\Service
 */
class CustomerService extends WebTestCase
{
    public function testCreateAddress()
    {
        $customer = $this->createCustomer();
        $repository = $this->mockCustomerRepository($customer);

        $addressService = new CustomerService($repository);
        $result = $addressService->create($customer);

        $this->assertInstanceOf(Customer::class, $result);
    }

    public function createCustomer()
    {
        $customer = new Customer();
        $customer->setFirstName('Jone')
            ->setLastName('Doe')
            ->setTelephone('004919878976')
        ;

        return $customer;
    }

    public function mockCustomerRepository(Customer $customer)
    {
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository
            ->expects($this->once())
            ->method('create')
            ->willReturn($customer)
        ;

        return $repository;
    }
}
